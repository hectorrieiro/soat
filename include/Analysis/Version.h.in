///
/// @file    ANALYSIS/Version.h
/// @brief   Report the version for this package.
/// @author  Hector Rieiro
///

#ifndef SACCADOUSTBIDESTIMULATION_ANALYSIS_VERSION_H
#define SACCADOUSTBIDESTIMULATION_ANALYSIS_VERSION_H

#include <string>
#include <sstream>

namespace Analysis {
/// The current major version.
#define ANALYSIS_VERSION_MAJOR @VERSION_MAJOR@

/// The current minor version.
#define ANALYSIS_VERSION_MINOR @VERSION_MINOR@

/// The current patch level.
#define ANALYSIS_VERSION_PATCH @VERSION_PATCH@

/// The current svn revision.
#define ANALYSIS_VCS_REVISION "@VCS_REVISION@"

/// True if the current version is newer than the given one.
#define ANALYSIS_VERSION_GT(MAJOR, MINOR, PATCH) \
  ((ANALYSIS_VERSION_MAJOR > MAJOR) ||           \
   (ANALYSIS_VERSION_MAJOR ==                    \
    MAJOR&&(ANALYSIS_VERSION_MINOR > MINOR || (ANALYSIS_VERSION_MINOR == MINOR&& ANALYSIS_VERSION_PATCH > PATCH))))

/// True if the current version is equal or newer to the given.
#define ANALYSIS_VERSION_GE(MAJOR, MINOR, PATCH) \
  ((ANALYSIS_VERSION_MAJOR > MAJOR) ||           \
   (ANALYSIS_VERSION_MAJOR ==                    \
    MAJOR&&(ANALYSIS_VERSION_MINOR > MINOR || (ANALYSIS_VERSION_MINOR == MINOR&& ANALYSIS_VERSION_PATCH >= PATCH))))

/// True if the current version is older than the given one.
#define ANALYSIS_VERSION_LT(MAJOR, MINOR, PATCH) \
  ((ANALYSIS_VERSION_MAJOR < MAJOR) ||           \
   (ANALYSIS_VERSION_MAJOR ==                    \
    MAJOR&&(ANALYSIS_VERSION_MINOR < MINOR || (ANALYSIS_VERSION_MINOR == MINOR&& ANALYSIS_VERSION_PATCH < PATCH))))

/// True if the current version is older or equal to the given.
#define ANALYSIS_VERSION_LE(MAJOR, MINOR, PATCH) \
  ((ANALYSIS_VERSION_MAJOR < MAJOR) ||           \
   (ANALYSIS_VERSION_MAJOR ==                    \
    MAJOR&&(ANALYSIS_VERSION_MINOR < MINOR || (ANALYSIS_VERSION_MINOR == MINOR&& ANALYSIS_VERSION_PATCH <= PATCH))))

/// Information about the current ProjA version.
class Version {
public:
  /// @return the current major version of Analysis.
  static int getMajor()
  {
    return ANALYSIS_VERSION_MAJOR;
  }

  /// @return the current minor version of Analysis.
  static int getMinor()
  {
    return ANALYSIS_VERSION_MINOR;
  }

  /// @return the current patch level of Analysis.
  static int getPatch()
  {
    return ANALYSIS_VERSION_PATCH;
  }

  /// @return the current Analysis version (MM.mm.pp).
  static std::string getString()
  {
    std::ostringstream version;
    version << ANALYSIS_VERSION_MAJOR << '.' << ANALYSIS_VERSION_MINOR << '.' << ANALYSIS_VERSION_PATCH;
    return version.str();
  }

  /// @return the SVN revision.
  static std::string getRevision()
  {
    return ANALYSIS_VCS_REVISION;
  }

  /// @return the current Analysis version plus the SVN revision (MM.mm.pp.rev).
  static std::string getRevString()
  {
    std::ostringstream version;
    version << getString() << '.' << ANALYSIS_VCS_REVISION;
    return version.str();
  }
};
}
}

#endif // SACCADOUSTBIDESTIMULATION_ANALYSIS_VERSION_H
